#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

#  Copyright 2008-2013 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""risto.py -- Robot Framework's Historical Reporting Tool

Version: <VERSION>

Usage:  risto.py options input_files
  or:   risto.py options1 --- options2 --- optionsN --- input files
  or:   risto.py --argumentfile path

risto.py plots graphs about test execution history based on statistics
read from Robot Framework output files. Actual drawing is handled by
Matplotlib tool, which must be installed separately.  More information
about it, including installation instructions, can be found from
http://matplotlib.sourceforge.net.

By default risto.py draws total, passed and failed graphs for critical
tests and all tests, but it is possible to omit some of these graphs
and also to add graphs by tags. Names of test rounds that are shown on
the x-axis are, by default, got from the paths to input files.
Alternatively, names can be got from the metadata of the top level
test suite (see Robot Framework's '--metadata' option for more details).

The graph is saved into a file specified with '--output' option, and the
output format is got from the file extension. Supported formats depend on the
installed Matplotlib back-ends, but at least PNG ought to be always available.
If the output file is omitted, the graph is opened into Matplotlib's image
viewer (which requires Matplotlib to be installed with some graphical
front-end).

It is possible to draw multiple graphs with different options at once. This
is done by separating different option groups with three or more hyphens
('---'). Note that in this case also paths to input files need to be
separated from last options similarly.

risto.py can also plots graphs by reading Robot Framework output files stored
in a Mercurial repository using the '--vcs' option. Each revision of the file
passed to '--vcs' is fetched from the Mercurial repository and processed by
risto.py.

Instead of giving all options from the command line, it is possible to read
them from a file specified with '--argument' option. In an argument file
options and their possible argument are listed one per line, and option
groups are separated with lines of three or more hyphens. Empty lines and
lines starting with a hash mark ('#') are ignored.

Options:
  -C --nocritical     Do not plot graphs for critical tests.
  -A --noall          Do not plot graphs for all tests.
  -T --nototals       Do not plot total graphs.
  -P --nopassed       Do not plot passed graphs.
  -F --nofailed       Do not plot failed graphs.
  -t --tag name *     Add graphs for these tags. Name can contain '*' and
                      '?' as wildcards.
  -o --output path    Path to the image file to create. If not given, the
                      image is opened into Matplotlib's image viewer.
  -i --title title    Title of the graph. Underscores in the given title
                      are converted to spaces. By default there is no
                      title.
  -w --width inches   Width of the image. Default is 800.
  -h --height inches  Height of the image. Default is 400.
  -f --font size      Font size used for legends and labels. Default is 8.
  -m --marker size    Size of marked used with tag graphs. Default is 5.
  -x --xticks num     Maximum number of ticks in x-axis. Default is 15.
  -n --namemeta name  Name of the metadata of the top level test suite
                      where to get name of the test round. By default names
                      are got from paths to input files.
  ---                 Used to group options when creating multiple images
                      at once.
  --vcs path          Output file stored in a VCS (Only Mercurial is
                      supported). Not working in multi-images mode.
  --argumentfile path  Read arguments from the specified file.
  --verbose           Verbose output.
  --help              Print this help.
  --version           Print version information.

Examples:
  risto.py --output history.png output1.xml output2.xml output3.xml
  risto.py --title My_Report --noall --namemeta Date --output out.png *.xml
  risto.py --nopassed --tag smoke --tag iter-* results/*/output.xml
  risto.py -CAP -t tag1 --- -CAP -t tag2 --- -CAP -t tag3 --- outputs/*.xml
  risto.py --argumentfile arguments.txt

     ====[arguments.txt]===================
     --title Overview
     --output overview.png
     ----------------------
     --nocritical
     --noall
     --nopassed
     --tag smoke1
     --title Smoke Tests
     --output smoke.png
     ----------------------
     path/to/*.xml
     ======================================
"""

# -----------------------------------------------------------------------------
# Module Import
# -------------
# 
from __future__ import with_statement
import os.path
import sys
import glob
import subprocess

try:
    from matplotlib import pylab
    from matplotlib.lines import Line2D
    from matplotlib.font_manager import FontProperties
    from matplotlib.pyplot import get_current_fig_manager
except ImportError:
    raise ImportError('Could not import Matplotlib modules. Install it form '
                      'http://matplotlib.sourceforge.net/')

try:
    from robot import utils
    from robot.errors import DataError, Information
except ImportError:
    raise ImportError('Could not import Robot Framework modules. '
                      'Make sure you have Robot Framework installed.')


__version__ = '1.0.2'


# -----------------------------------------------------------------------------
# Risto
# -----
# 
class AllStatistics(object):

    def __init__(self, paths, namemeta=None, verbose=False):
        """Constructor.
        
        :Parameters:
          - `paths`: seq. List of tuples of length 2. Each tuple represents a Robot Framework output file. The
            first element is the name of the file. The second item is the path to the file or an opened file
            object.
          - `namemeta`: string. If specified, the output files are expected to contain metadata with the given
            name. The value of this metadata is used as the name. If not specified, the name specified in
            "paths" is used.
          - `verbose`: bool. If True, print debugging information. Default is False.
        
        """
        
        self._stats = self._get_stats(paths, namemeta, verbose)
        self._tags = self._get_tags()

    def _get_stats(self, paths, namemeta, verbose):
        if namemeta:
            return [Statistics(path, namemeta=namemeta, verbose=verbose)
                    for name, path in paths]
        return [Statistics(path, name, verbose=verbose)
                for name, path in paths]

    def _get_tags(self):
        stats = {}
        for statistics in self._stats:
            stats.update(statistics.tags)
        return [stat.name for stat in sorted(stats.values())]

    def plot(self, plotter):
        plotter.set_axis(self._stats)
        plotter.critical_tests([s.critical_tests for s in self._stats])
        plotter.all_tests([s.all_tests for s in self._stats])
        for tag in self._tags:
            plotter.tag([s[tag] for s in self._stats])


class Statistics(object):

    def __init__(self, path, name=None, namemeta=None, verbose=False):
        if verbose:
            print path
        root = utils.ET.ElementTree(file=path).getroot()
        self.name = self._get_name(name, namemeta, root)
        stats = root.find('statistics')
        crit_node, all_node = list(stats.find('total'))
        self.critical_tests = Stat(crit_node)
        self.all_tests = Stat(all_node)
        self.tags = dict((n.text, Stat(n)) for n in stats.find('tag'))

    def _get_name(self, name, namemeta, root):
        if namemeta is None:
            if name is None:
                raise TypeError("Either 'name' or 'namemeta' must be given")
            return name
        metadata = root.find('suite').find('metadata')
        if metadata:
            for item in metadata:
                if item.get('name','').lower() == namemeta.lower():
                    return item.text
        raise DataError("No metadata matching '%s' found" % namemeta)

    def __getitem__(self, name):
        try:
            return self.tags[name]
        except KeyError:
            return EmptyStat(name)


class Stat(object):

    def __init__(self, node):
        self.name = node.text
        self.passed = int(node.get('pass'))
        self.failed = int(node.get('fail'))
        self.total = self.passed + self.failed
        self.doc = node.get('doc', '')
        info = node.get('info', '')
        self.critical = info == 'critical'
        self.non_critical = info == 'non-critical'
        self.combined = info == 'combined'

    def __cmp__(self, other):
        if self.critical != other.critical:
            return self.critical is True and -1 or 1
        if self.non_critical != other.non_critical:
            return self.non_critical is True and -1 or 1
        if self.combined != other.combined:
            return self.combined is True and -1 or 1
        return cmp(self.name, other.name)


class EmptyStat(Stat):

    def __init__(self, name):
        self.name = name
        self.passed = self.failed = self.total = 0
        self.doc = ''
        self.critical = self.non_critical = self.combined = False


class Legend(Line2D):

    def __init__(self, **attrs):
        styles = {'color': '0.5', 'linestyle': '-', 'linewidth': 1}
        styles.update(attrs)
        Line2D.__init__(self, [], [], **styles)


class Plotter(object):
    _total_color = 'blue'
    _pass_color = 'green'
    _fail_color = 'red'
    _background_color = '0.8'
    _xtick_rotation = 20
    _default_width = 800
    _default_height = 400
    _default_font = 8
    _default_marker = 5
    _default_xticks = 15
    _dpi = 100
    _marker_symbols = 'o s D ^ v < > d p | + x 1 2 3 4 . ,'.split()

    def __init__(self, tags=None, critical=True, all=True, totals=True,
                 passed=True, failed=True, width=None, height=None, font=None,
                 marker=None, xticks=None):
        self._xtick_limit, self._font_size, self._marker_size, width, height  \
               = self._get_sizes(xticks, font, marker, width, height)
        self._figure = pylab.figure(figsize=(width, height))
        self._axes = self._figure.add_axes([0.05, 0.15, 0.65, 0.70])
        # axes2 is used only for getting ytick labels also on right side
        self._axes2 = self._axes.twinx()
        self._axes2.set_xticklabels([], visible=False)
        self._tags = tags or []
        self._critical = critical
        self._all = all
        self._totals = totals
        self._passed = passed
        self._failed = failed
        self._legends = []
        self._markers = iter(self._marker_symbols)

    def _get_sizes(self, xticks, font, marker, width, height):
        xticks = xticks or self._default_xticks
        font   = font   or self._default_font
        marker = marker or self._default_marker
        width  = width  or self._default_width
        height = height or self._default_height
        try:
            return (int(xticks), int(font), int(marker),
                    float(width)/self._dpi, float(height)/self._dpi)
        except ValueError:
            raise DataError('Width, height, font and xticks must be numbers.')

    def set_axis(self, stats):
        slen = len(stats)
        self._indexes = range(slen)
        self._xticks = self._get_xticks(slen, self._xtick_limit)
        self._axes.set_xticks(self._xticks)
        self._axes.set_xticklabels([stats[i].name for i in self._xticks],
                                   rotation=self._xtick_rotation,
                                   size=self._font_size)
        self._scale = (slen-1, max(s.all_tests.total for s in stats))

    def _get_xticks(self, slen, limit):
        if slen <= limit:
            return range(slen)
        interval, extra = divmod(slen-1, limit-1)  # 1 interval less than ticks
        if interval < 2:
            interval = 2
            limit, extra = divmod(slen-1, interval)
            limit += 1
        return [ self._get_index(i, interval, extra) for i in range(limit) ]

    def _get_index(self, count, interval, extra):
        if count < extra:
            extra = count
        return count * interval + extra

    def critical_tests(self, stats):
        if self._critical:
            line = {'linestyle': '--', 'linewidth': 1}
            self._plot(self._indexes, stats, **line)
            self._legends.append(Legend(label='critical tests', **line))

    def all_tests(self, stats):
        if self._all:
            line = {'linestyle': ':', 'linewidth': 1}
            self._plot(self._indexes, stats, **line)
            self._legends.append(Legend(label='all tests', **line))

    def tag(self, stats):
        if utils.MultiMatcher(self._tags).match(stats[0].name):
            line = {'linestyle': '-', 'linewidth': 0.3}
            mark = {'marker': self._get_marker(),
                    'markersize': self._marker_size}
            self._plot(self._indexes, stats, **line)
            markers = [stats[index] for index in self._xticks]
            self._plot(self._xticks, markers, linestyle='', **mark)
            line.update(mark)
            label = self._get_tag_label(stats)
            self._legends.append(Legend(label=label, **line))

    def _get_tag_label(self, stats):
        label = stats[0].name
        # need to go through all stats because first can be EmptyStat
        for stat in stats:
            if stat.critical:
                return label + ' (critical)'
            if stat.non_critical:
                return label + ' (non-critical)'
        return label

    def _get_marker(self):
        try:
            return self._markers.next()
        except StopIteration:
            return ''

    def _plot(self, xaxis, stats, **attrs):
        total, passed, failed \
               = zip(*[(s.total, s.passed, s.failed) for s in stats])
        if self._totals:
            self._axes.plot(xaxis, total, color=self._total_color, **attrs)
        if self._passed:
            self._axes.plot(xaxis, passed, color=self._pass_color, **attrs)
        if self._failed:
            self._axes.plot(xaxis, failed, color=self._fail_color, **attrs)

    def draw(self, output=None, title=None):
        self._set_scale(self._axes)
        self._set_scale(self._axes2)
        self._set_legends(self._legends[:])
        if title:
            title = title.replace('_', ' ')
            self._axes.set_title(title, fontsize=self._font_size*1.8)
        if output:
            self._figure.savefig(output, facecolor=self._background_color,
                                 dpi=self._dpi)
        else:
            if not hasattr(self._figure, 'show'):
                raise DataError('Could not find a graphical front-end for '
                                'Matplotlib.')
            self._figure.show()
            if title:
                figman = get_current_fig_manager()
                figman.set_window_title(title)

    def _set_scale(self, axes):
        width, height = self._scale
        axes.axis([-width*0.01, width*1.01, -height*0.04, height*1.04])

    def _set_legends(self, legends):
        legends.insert(0, Legend(label='Styles:', linestyle=''))
        legends.append(Legend(label='', linestyle=''))
        legends.append(Legend(label='Colors:', linestyle=''))
        if self._totals:
            legends.append(Legend(label='total', color=self._total_color))
        if self._passed:
            legends.append(Legend(label='passed', color=self._pass_color))
        if self._failed:
            legends.append(Legend(label='failed', color=self._fail_color))
        labels = [l.get_label() for l in legends]
        self._figure.legend(legends, labels, loc='center right',
                            numpoints=3, borderpad=0.1,
                            prop=FontProperties(size=self._font_size))


class Ristopy(object):

    def __init__(self):
        self._vcs = [Mercurial()]
        self._arg_parser = utils.ArgumentParser(__doc__, version=__version__)

    def main(self, args):
        args = self._process_possible_argument_file(args)
        try:
            opt_groups, paths = self._split_to_option_groups_and_paths(args)
        except ValueError:
            opts, paths = self._arg_parser.parse_args(args)
            paths = glob_paths(paths)
            sources = zip(get_names(paths), paths) + self._process_vcs_opt(opts)
            if not sources:
                raise DataError("No valid sources given.")
            viewer_open = self._plot_one_graph(opts, sources)
        else:
            # TODO: fix multi-images mode when --vcs is used
            paths = glob_paths(paths)
            if not sources:
                raise DataError("No valid sources given.")
            viewer_open = self._plot_multiple_graphs(opt_groups, zip(get_names(paths), paths))

        if viewer_open:
            try:
                raw_input('Press enter to exit.\n')
            except (EOFError, KeyboardInterrupt):
                pass
            pylab.close('all')
    
    def _process_vcs_opt(self, opts):
        """Process value passed to the '--vcs' option.
        
        :Parameters:
          - `opts`: dict. Options passed to risto.py.
        
        :return: list. Additional sources that must be includes in the graph. Each source is a tuple of
                 length 2. First element is the name of the report and the second one is the result data
                 ("output.xml" file content).
        
        """
        
        sources = []
        if opts['vcs']:
            for vcs in self._vcs:
                if not vcs.file_tracked(opts['vcs']):
                    continue
                
                for changeset in vcs.changesets(opts['vcs']):
                    sources.append(( changeset['changeset']
                                   , vcs.file_content(opts['vcs'], changeset['changeset'])
                                   ))
        
        return sources

    def _plot_one_graph(self, opts, paths):
        stats = AllStatistics(paths, opts['namemeta'], opts['verbose'])
        output = self._plot(stats, opts)
        return output is None

    def _plot_multiple_graphs(self, opt_groups, paths):
        viewer_open = False
        stats = AllStatistics(paths, opt_groups[0]['namemeta'],
                              opt_groups[0]['verbose'])
        for opts in opt_groups:
            output = self._plot(stats, opts)
            viewer_open = output is None or viewer_open
        return viewer_open

    def _plot(self, stats, opts):
        plotter = Plotter(opts['tag'],  not opts['nocritical'],
                          not opts['noall'], not opts['nototals'],
                          not opts['nopassed'], not opts['nofailed'],
                          opts['width'], opts['height'], opts['font'],
                          opts['marker'], opts['xticks'])
        stats.plot(plotter)
        plotter.draw(opts['output'], opts['title'])
        if opts['output']:
            print os.path.abspath(opts['output'])
        return opts['output']

    def _process_possible_argument_file(self, args):
        try:
            index = args.index('--argumentfile')
        except ValueError:
            return args
        path = args[index+1]
        try:
            lines = open(path).readlines()
        except IOError:
            raise DataError("Invalid argument file '%s'" % path)
        fargs = []
        for line in lines:
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            elif line.startswith('-'):
                fargs.extend(line.split(' ', 1))
            else:
                fargs.append(line)
        args[index:index+2] = fargs
        return args

    def _split_to_option_groups_and_paths(self, args):
        opt_groups = []
        current = []
        for arg in args:
            if arg.replace('-', '') == '' and len(arg) >= 3:
                opts = self._arg_parser.parse_args(current)[0]
                opt_groups.append(opts)
                current = []
            else:
                current.append(arg)
        if opt_groups:
            return opt_groups, current
        raise ValueError("Nothing to split")

# -----------------------------------------------------------------------------
# Utilities
# ---------
# 

def glob_paths(orig):
    """Return list of existing files matched by given path patterns.
    
    :Parameters:
      - `orig`: seq. List of path patterns to be expanded.
    
    :return: list. List of existing files matched by given patterns.
    
    """
    
    paths = []
    for path in orig:
        paths.extend(glob.glob(path))
    return paths

def get_names(paths):
    """Generate report name based on Robot Framework output file path.
    
    :Parameters:
      - `paths`: seq. List of paths from which report name must be generated.
    
    :return: list. List of report names generated from output file paths in the same order that paths were
             given.
    
    
    **Examples**
    
    Typical usage:
    
      >>> output_files = ['/home/user/output1.xml', '/home/user/output2.xml', '/home/user/output3.xml']
      
      >>> get_names(output_files)
      ['Output 1', 'Output 2', 'Output 3']
      
      >>> zip(get_names(output_files), output_files)
      [('Output 1', '/home/user/output1.xml'), ('Output 2', '/home/user/output2.xml'), ('Output 3', '/home/user/output3.xml')]
    
      >>> get_names(['/home/user/output.xml', '/home/a_user/output.xml', '/home/another_user/output.xml'])
      ['User', 'A User', 'Another User']
    
    Work with Windows paths as well:
    
      >>> get_names([ 'c:\\test_results\\output1.xml'
      ...           , 'c:\\test_results\\output2.xml'
      ...           , 'c:\\test_results\\output3.xml'
      ...           ])
      ['Output 1', 'Output 2', 'Output 3']
    
    
    No path validation is done. Empty strings are replaced by 'Ristopy':
    
      >>> get_names(['', 'Hello World!', '!@#$', 'a_string'])
      ['Ristopy', 'Hello World!', '!@#$', 'A String']
      >>> get_names([])
      []
    
    
    """
    
    if not paths:
        return []
    
    paths = [os.path.splitext(os.path.abspath(p))[0] for p in paths]
    path_tokens = [p.replace('\\', '/').split('/') for p in paths]
    min_tokens = min(len(t) for t in path_tokens)
    index = -1
    while tokens_are_same_at_index(path_tokens, index):
        index -= 1
        if abs(index) > min_tokens:
            index = -1
            break
    names = [tokens[index] for tokens in path_tokens]
    return [utils.printable_name(n, code_style=True) for n in names]

def tokens_are_same_at_index(token_list, index):
    """Determine if the token at a given index is the same in all tokens lists.
    
    :Parameters:
      - `token_list`: seq. List of list of tokens.
      - `index`: int. Index of the token to check.
    
    :return: bool. True if the token at the given index is the same in all tokens lists.
    :raise IndexError: 'token_list' is empty or 'index' is bigger than the length of the smallest list.
    
    
    **Examples**
    
    
    Work with any types:
    
      >>> import string
      >>> tokens_are_same_at_index([string.ascii_lowercase, string.ascii_lowercase], 10)
      True
      >>> tokens_are_same_at_index([range(3), range(3)], 2)
      True
    
    
    If 'token_list' contains one item, tokens_are_same_at_index() always returns False:
    
      >>> tokens_are_same_at_index(['hello world'], 0)
      False
    
    
    Index must be lower than the length of the smallest list:
    
      >>> tokens_are_same_at_index([range(3), range(4)], 3)
      Traceback (most recent call last):
          ...
      IndexError: list index out of range
    
    
    Passing an empty tokens list results in an IndexError:
    
      >>> tokens_are_same_at_index([], 0)
      Traceback (most recent call last):
          ...
      IndexError: list index out of range
    
    """
    
    first = token_list[0][index]
    for tokens in token_list[1:]:
        if first != tokens[index]:
            return False
    return len(token_list) > 1

class LazyStream(object):
    """Stream that is not created until it is read.
    
    """
    
    def __init__(self, callable_obj, *args, **kwargs):
        """Constructor.
        
        :Parameters:
          - `callable_obj`: callable object. A callable object that returns one thing, a file-like object
            that has a read() method. Calls to `LazyStream.read()` are redirected to this object.
          - `args`: seq. Arguments to be passed to "callable_obj".
          - `kwargs`: dict. Keyword arguments to be passed to "callable_obj".
        
        """
        
        self._callable = callable_obj
        self._args = args
        self._kwargs = kwargs
        self._fd = None
    
    def read(self, size=None):
        """Read from the stream. Create the file-like object the first time read() is called.
        
        :Parameters:
          - `size`: int. Number of bytes to read from the stream. If not specified, the stream is read until
            EOF is read.
        
        :return: string. Content read from the stream.
        
        """
        
        if not self._fd:
            self._fd = self._callable(*self._args, **self._kwargs)
        
        if size:
            return self._fd.read(size)
        
        return self._fd.read()

# -----------------------------------------------------------------------------
# VCS Support
# -----------
# 

class VCSError(Exception):
    """Error during VCS.
    
    """

class VCS(object):
    """VCS base class. Exist for the sole purpose of documenting the interface that VCS sub-classes are
    expected to implement.
    
    """
    
    def changesets(self, path):
        """Return the list of changesets affecting a file.
        
        :Parameters:
          - `path`: string. Path to the file for which changesets affecting it must be collected.
        
        :return: seq. List of changeset affecting the specified file. Can be a generator.
        
        """
        
        raise NotImplementedError()
    
    def file_content(self, path, rev=None):
        """Return the content of a file at a given revision.
        
        :Parameters:
          - `path`: string. Path to the file for which the content at the given revision must be fetched.
          - `rev`: string or None. The content of the file at this revision revision. If not specified, the content of the latest revision of the file is returned.
        
        :return: string. Content of the file at the given revision. Consider to return a generator that yield
                 file content. Otherwise, the whole content of each revision of the file will be loaded in
                 memory with current Risto implementation.
        
        """
        
        raise NotImplementedError()
    
    def file_tracked(self, path):
        """Determine if a given file is tracked or not by this VCS.
        
        :Parameters:
          - `path`: string. Path to the file for which we must determine if it is tracked or not.
        
        :return: bool. True if the specified file is tracked byt his VCS, False otherwise.
        
        """
        
        raise NotImplementedError()

class Mercurial(VCS):
    """`Mercurial` is a DVCS written in Python.
    
    .. _Mercurial: http://mercurial.selenic.com/
    
    """
    
    def changesets(self, path, limit=None):
        """Retrieve revision history of entire repository or files.
        
        :Parameters:
          - `path`: string. Path to en
          - `limit`: string or int. Maximum number of changesets to return.
          - `key`: string. If specified, must be one of: 'changeset', 'tag', 'user', 'date' or 'summary'. Only
            the value of this key will be included in the changeset.
        
        :return: list of dictionary. Each dictionary represents a changeset. See `MercurialCLI._parse_log()`
                 for available keys.
        :raise MercurialNotFoundError: when Mercurial hg command is not found.
        :raise Error: when the current directory is not inside a Mercurial repository.
        
        
        Extract the list of changesets returned by the commands hg tip or hg log. Should work with any
        Mercurial commands that print changesets information but it is only tested with hg tip and hg log for
        now.
        
        :Parameters:
          - `command`: list of strings; the 'log' or 'tip' command to execute.
        
        :return: list of dictionary. Each dictionary represents a changeset. Keys of changeset dictionary are:
        
          - changeset:      Changeset number.
          - user:           Name of the commiter. Usual format of this string is
                            "first_name last_name <email>" but the user may have entered it differently.
          - date:           Date at which the change was committed.
          - summary:        Description of the changes.
          - tag (optional): Tag(s) associated to this changeset.
        
        :raise MercurialNotFoundError: when Mercurial hg command is not found.
        :raise Error: when the current directory is not inside a Mercurial repository.
        
        """
        
        command = [ 'hg', 'log'
                  , '-r', ':'    # Report changesets in reverse order (oldest to newest).
                  ]
        
        if limit:
            command += ['-l', str(limit)]
        
        command.append(path)
        
        changeset = {}
        
        hg_process = subprocess.Popen( command
                                     , stdout=subprocess.PIPE
                                     , stderr=subprocess.STDOUT
                                     , shell=True
                                     , cwd=os.path.dirname(path)
                                     )
        
        for line in hg_process.stdout:
            line = line.strip()
            
            # skip empty line or line that has no period
            if not line or (':' not in line):
                continue
            
            # extract section and value
            section, value = line.split(':', 1)
            
            # remove quotes from value
            value = value.strip()
            if value.startswith('"'):
                value = value[1:-1]
            
            # store the info
            changeset[section] = value
            
            # 'summary' is the last piece of information printed of a changeset. Once we got it, store the
            # changeset and look for the next one.
            if section == 'summary':
                yield changeset
                changeset = {}
    
    def file_content(self, path, rev=None):
        """Output the given revision of file.
        
        :Parameters:
          - `path`: string. Path to the filename to print.
          - `rev`: string. Print the specified files as they were at the given revision. If no revision is
            given, the parent of the working directory is used, or tip if no revision is checked out.
        
        :return: LazyStream object. 
        
        """
        
        command = ['hg', 'cat']
        
        if rev:
            command += ['-r', str(rev)]
        
        command.append(path)
        
        def run_command():
            return subprocess.Popen( command
                                   , stdout=subprocess.PIPE
                                   , stderr=subprocess.STDOUT
                                   , shell=True
                                   , cwd=os.path.dirname(path)
                                   ).stdout
        
        # Run the command.
        return LazyStream(run_command)
    
    def file_tracked(self, path):
        """Determine if the specified file is tracked by Mercurial.
        
        :Parameters:
          - `path`: string. Path to the file for which we must determine if it is tracked by Mercurial or not.
        
        :return: bool. True if the specified file is tracked by Mercurial, False otherwise.
        
        """
        
        # First, check if the file is inside a Mercurial repository.
        head = path
        tail = True
        while tail:
            head, tail = os.path.split(head)
            if os.path.isdir(os.path.join(head, '.hg')):
                break
        else:
            return False
        
        # Secondly, check if Mercurial tracks the file.
        output = subprocess.check_output(['hg', 'status', '-A', path], shell=True)
        if not output:
            raise VCSError('Mercurial cannot determines the status of the file "%s"' % path)
        
        return output[0] in [ 'M'    # modified
                            , 'A'    # added
                            , 'C'    # clean
                            ]

# -----------------------------------------------------------------------------
# Script Execution
# ----------------
# 
if __name__ == '__main__':
    try:
        Ristopy().main(sys.argv[1:])
    except Information, msg:
        print str(msg)
    except DataError, err:
        print '%s\n\nTry --help for usage information.' % err
